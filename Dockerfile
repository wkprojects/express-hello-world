FROM node:13-alpine

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . .

RUN npm install

ENV PORT 5000
EXPOSE 5000

CMD ["node", "bin/www"]
